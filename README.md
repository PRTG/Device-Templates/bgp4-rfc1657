## PRTG Device Template for monitoring BGP v4 Peers via SNMP with PRTG

This project is a custom device template that can be used to monitor any device that implements the BGP4-MIB (RFC-1657) in PRTG using the auto-discovery for simplified sensor creation.

## Download
A zip file containing the template and necessary additional files can be downloaded from the repository using the link below:
- [Latest Version of the Template](https://gitlab.com/PRTG/Device-Templates/bgp4-rfc1657/-/jobs/artifacts/master/download?job=PRTGDistZip)

## Installation Instructions
Please refer to INSTALL.md or refer to the following KB-Post:
- [Sensor for BGP status](https://kb.paessler.com/en/topic/25313)